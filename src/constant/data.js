const DYMMY_DATA = [
  {
    id: 1,
    name: "ReactJS",
    description:
      "ReactJS is a JavaScript library for building user interfaces.",
    image:
      "https://devapi.softylines.com/uploads/REACTJ_Sreactjs_3edeb82063.svg",
  },
  {
    id: 2,
    name: "NodeJS",
    description:
      "Node.js is an open-source, cross-platform, back-end JavaScript runtime environment that runs on the V8 engine and executes JavaScript code outside a web browser.",
    image:
      "https://devapi.softylines.com/uploads/linkedin_thumb_node_1_a88a39b140.svg",
  },
  {
    id: 3,
    name: "JavaScript",

    description:
      "JavaScript, often abbreviated as JS, is a programming language that conforms to the ECMAScript specification.",
    image: "https://devapi.softylines.com/uploads/Group_94js_4c02e35057.svg",
  },
  {
    id: 4,
    name: "NextJs",
    description:
      "Next.js is an open-source React front-end development web framework that enables functionality such as server-side rendering and generating static websites for React based web applications.",
    image:
      "https://devapi.softylines.com/uploads/next_js_icon_logo_EE_302_D5_DBD_seeklogo_1next_2c50d82125.svg",
  },
];
export default DYMMY_DATA;
