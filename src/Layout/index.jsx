import React from "react";
import Header from "./Header";

function Layout() {
  return (
    <div className="layout">
      <Header />
      <div className="container">
        {/* TODO Add code to allow reading pages here */}
      </div>
    </div>
  );
}

export default Layout;
