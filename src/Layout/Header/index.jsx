import React from "react";

function Header() {
  return (
    <div className="header">
      <div className="logo-wrapper">
        <img
          className="logo"
          src="https://dev.softylines.com/_next/image?url=%2FsoftylinesLogo.png&w=96&q=75"
          alt="logo"
        />
        <img
          className="logo-name"
          src="https://dev.softylines.com/_next/image?url=%2FsoftylinesLogo2.png&w=256&q=75"
          alt="logo name"
        />
      </div>

      <nav>
        <ul className="nav-list">
          <li className="nav-item">
            {/* Navlink  */}
            Home
            {/* Navlink  */}
          </li>
          <li className="nav-item">
            {/* Navlink  */}
            About
            {/* Navlink  */}
          </li>
          <li className="nav-item">
            {/* Navlink  */}
            Technologies
            {/* Navlink  */}
          </li>
        </ul>
      </nav>
    </div>
  );
}

export default Header;
