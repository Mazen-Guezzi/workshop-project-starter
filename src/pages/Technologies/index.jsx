import React from "react";
import { useNavigate } from "react-router-dom";
import DYMMY_DATA from "../../constant/data";

function Technologies() {
  const navigate = useNavigate();

  return (
    <div className="technologies">
      <div className="content">
        <p className="page-header">Technologies</p>
        <h1 className="sub-title">
          Latest Tools And Techniques To Drive Your Business Forward.
        </h1>
        <p className="description">
          At Softylines, We Address Complex Challenges Through The Use Of
          Traditional And Cutting-Edge Technologies To Provide Our Customers
          With A Seamless Development Experience.
        </p>
        <button className="contact-us">Get Free Consultation</button>
        <div className="all-tech">
          {DYMMY_DATA.map((technologie) => {
            return (
              <div
                className="technologie-img"
                onClick={() => {
                  navigate(`${technologie.id}`);
                }}
              >
                <img src={technologie.image} alt={technologie.image} />
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
}

export default Technologies;
