import React from "react";
import DYMMY_DATA from "../../../constant/data";
import { useNavigate, useParams } from "react-router-dom";

function Technologie() {
  const params = useParams();
  const selectedTech = DYMMY_DATA.find((tech) => tech.id === +params.id);
  const navigate = useNavigate();
  return (
    <div className="technologie">
      <div className="arrow-back" onClick={() => navigate(-1)}>
        <img
          src="https://www.takiacademy.com/_next/image?url=%2F_next%2Fstatic%2Fmedia%2Fleft-arrow.18e42944.png&w=64&q=75"
          alt="arrow-back"
        />
      </div>

      <div className="content">
        <h1>{selectedTech.name}</h1>
        <p>{selectedTech.description}</p>
        <img src={selectedTech.image} alt={selectedTech.name} />
      </div>
    </div>
  );
}

export default Technologie;
