import React from "react";

function About() {
  return (
    <div className="about">
      <img
        src="https://raw.githubusercontent.com/pico-india/main-django/main/static/about-team.jpg"
        alt=""
      />
    </div>
  );
}

export default About;
