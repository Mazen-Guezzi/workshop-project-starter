import React from "react";

function Home() {
  return (
    <div className="home">
      <div className="content">
        <p className="speciality">software development company</p>
        <h1 className="goals">End-To-End Software Products</h1>
        <p className="description">
          Welcome To Softylines, Where Innovation And Expertise Collide. As A
          Software Development Company, We Pride Ourselves On Delivering
          Customized, End-To-End Solutions To Businesses Of All Sizes.
        </p>
      </div>
      <div className="img">
        <img
          src="https://devapi.softylines.com/uploads/Group_4541computer_d6d0cbbb09.svg"
          alt="computer"
        />
      </div>
    </div>
  );
}

export default Home;
